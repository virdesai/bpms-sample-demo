pipeline {
  agent any

  options {
    buildDiscarder(logRotator(numToKeepStr:'5'))
    disableConcurrentBuilds()
    skipStagesAfterUnstable()
    checkoutToSubdirectory('hello-world')
  }

  environment {
      MVN_HOME = tool 'Maven3'
  }

  stages {

    stage ('Prepare') {
      steps {
        deleteDir()
        checkout scm
        script {
          // Validate last commit wasn't through CI Release process.
          isCiSkip = sh (script: "git log -1 | grep '.*\\[ci skip\\].*'",
            returnStatus: true)

          // commit information
          branchName = "${env.BRANCH_NAME}"
          commitId = sh(script: 'git rev-parse HEAD', returnStdout: true).trim()
          commitDate = sh(
            script: "git show -s --format=%cd --date=format:%Y%M%d-%H%m%s ${commitId}",
            returnStdout: true
          )

          dir ('hello-world') {
            pom = readMavenPom file: 'pom.xml'

            // Modify POM Version if not master/dev/release
            if (!(branchName ==~ /^(master|dev|release-)$/)) {
              def pomVersion = pom.version.replace(
                "-SNAPSHOT", "-${BRANCH_NAME}-SNAPSHOT")
              configFileProvider([configFile(
                fileId: '88a932c8-1e91-40f9-a09d-d1598ca6c6a6',
                variable: 'MVN_SETTINGS')]) {
                sh "mvn versions:set -DnewVersion=${pomVersion} -s ${MVN_SETTINGS}"
              }
              pom = readMavenPom file: 'pom.xml' // Retrieve pom with updated version
            }
          }

          // retrieve pom information
          buildVersion = pom.version.replace("-SNAPSHOT",
            ".${commitDate}.${commitId.substring(0, 7)}")
          currentBuild.displayName =
            "#${currentBuild.number} - ${buildVersion} - ${BRANCH_NAME}"
          releaseVersion = pom.version.replace("-SNAPSHOT", "")
        }
      }
    }

    // Build kjar artifact & run unit testing.
    stage ('Build') {
      when { expression { return isCiSkip } }
      steps {
        echo "Build kjar artifact for ${pom.version}"
        configFileProvider([configFile(
          fileId: '88a932c8-1e91-40f9-a09d-d1598ca6c6a6',
          variable: 'MVN_SETTINGS')]) {
          dir ('hello-world') {
            sh "mvn -s ${MVN_SETTINGS} clean package -U"
          }
        }
      }
    }

    // Uploading the snapshot to nexus
    stage ('Upload Snapshot artifact') {
      when {
        not { branch 'master'}
        expression { return isCiSkip }
      }
      steps {
        echo "Upload snapshot artifact for ${pom.groupId}:${pom.artifactId}:${pom.version}"
        configFileProvider([configFile(
          fileId: '88a932c8-1e91-40f9-a09d-d1598ca6c6a6',
          variable: 'MVN_SETTINGS')]) {
          dir ('hello-world') {
            sh "mvn -s ${MVN_SETTINGS} deploy -DskipTests=true"
          }
        }
      }
    }

    // Integration Testing
    stage ('Integration Testing') {
      when {
        not { branch 'master'}
        expression { return isCiSkip }
      }
      steps {
        echo 'Run integration testing if any.'
      }
    }

    // Question for delivery & releasing
    stage ('Continuous Delivery Request') {
      when {
        branch 'dev'
        expression { return isCiSkip }
      }
      steps {
        script {
          isRelease = false
          isDeploy = false
          try {
            // Send notification that Continuous Delivery Request is being made.
            // Should increase the timeout based on how long wait is needed.
            timeout(time: 30, unit: 'SECONDS') {
              input 'Do you want to release?'
              isRelease = true
            }
            timeout(time: 15, unit: 'SECONDS') {
              input'Do you want to deploy to production?'
              isDeploy = true
            }
          } catch (error) {
            // Not an error, just request for release.
          }
        }
      }
    }

    // Only release on dev branch
    stage ('Perform Release') {
      when {
        branch 'dev'
        expression { return isCiSkip }
        expression { return isRelease }
      }
      steps {
        deleteDir()
        checkout scm
        configFileProvider([configFile(
          fileId: '88a932c8-1e91-40f9-a09d-d1598ca6c6a6',
          variable: 'MVN_SETTINGS')]) {
          dir ('hello-world') {
            sh "mvn -B -s ${MVN_SETTINGS} jgitflow:release-start -DskipTests=true"
            sh "mvn -B -s ${MVN_SETTINGS} jgitflow:release-finish -DskipTests=true"
          }
        }
      }
    }

    // Deploy to production if on dev branch and dev branch has been released.
    stage ('Deploy to Production') {
      when {
        branch 'dev'
        expression { return isCiSkip }
        expression { return isDeploy }
      }
      steps {
        withCredentials([usernameColonPassword(credentialsId: 'bpms-prod', variable: 'USERPASS')]) {
          script {
            deployResponseCode = sh(
              script: "curl -w %{http_code} -s -o /dev/null -v -u $USERPASS -X POST -d '{}' -H 'Content-Type: application/json' http://${BPMS_PROD}/business-central/rest/deployment/${pom.groupId}:${pom.artifactId}:${releaseVersion}/deploy",
              returnStdout: true
            ).trim()
            if (!deployResponseCode.equals("202")) {
              error("BPM Suite deployment failed with response code: ${deployResponseCode}")
            }
          }
        }
      }
    }
  }
}
